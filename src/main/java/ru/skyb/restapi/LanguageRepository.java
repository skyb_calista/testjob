package ru.skyb.restapi;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.skyb.restapi.model.Language;

@Repository
public interface LanguageRepository extends CrudRepository<Language, Long> {
    Language findByName(String name);
}
