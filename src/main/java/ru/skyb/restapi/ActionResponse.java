package ru.skyb.restapi;

import com.fasterxml.jackson.annotation.JsonInclude;
import ru.skyb.restapi.model.Language;

public class ActionResponse {
    private String state;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Language body;

    public ActionResponse(String state) {
        this.state = state;
    }

    public ActionResponse(String state, Language body) {
        this.state = state;
        this.body = body;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Language getBody() {
        return body;
    }

    public void setBody(Language body) {
        this.body = body;
    }
}