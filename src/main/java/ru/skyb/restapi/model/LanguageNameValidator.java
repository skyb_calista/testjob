package ru.skyb.restapi.model;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Arrays;
import java.util.List;

public class LanguageNameValidator implements ConstraintValidator<LanguageName, String> {

    private final List<String> allowedLanguages = Arrays.asList("Java", "JavaScript", "C#", "C++", "Python");

    @Override
    public boolean isValid(String name, ConstraintValidatorContext constraintValidatorContext) {
        return allowedLanguages.contains(name);
    }
}
