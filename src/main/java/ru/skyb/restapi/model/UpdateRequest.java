package ru.skyb.restapi.model;


import org.springframework.lang.Nullable;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

public class UpdateRequest {

    @Nullable
    private String description;
    @Nullable
    @Min(value = 1)
    @Max(value = 5)
    private Integer rating;

    @Nullable
    public String getDescription() {
        return description;
    }

    public void setDescription(@Nullable String description) {
        this.description = description;
    }

    @Nullable
    public Integer getRating() {
        return rating;
    }

    public void setRating(@Nullable Integer rating) {
        this.rating = rating;
    }
}
