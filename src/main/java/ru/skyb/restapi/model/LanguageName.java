package ru.skyb.restapi.model;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target({ FIELD })
@Retention(RUNTIME)
@Constraint(validatedBy = LanguageNameValidator.class)
@Documented
public @interface LanguageName {
    String message() default "Unsupported language";

    Class<?>[] groups() default { };

    Class<? extends Payload>[] payload() default { };

}