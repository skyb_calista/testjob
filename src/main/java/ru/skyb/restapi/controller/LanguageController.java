package ru.skyb.restapi.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.skyb.restapi.model.Language;
import ru.skyb.restapi.LanguageRepository;
import ru.skyb.restapi.ActionResponse;
import ru.skyb.restapi.model.UpdateRequest;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
public class LanguageController {

    private final static Logger logger = LoggerFactory.getLogger(LanguageController.class);
    private final LanguageRepository languageRepository;

    @Autowired
    public LanguageController(LanguageRepository languageRepository) {
        this.languageRepository = languageRepository;
    }

    @GetMapping(value = {"/language"})
    public List<Language> getAll() {
        List<Language> languages = new ArrayList<>();
        languageRepository.findAll().forEach(languages::add);
        return languages;
    }

    @RequestMapping(value = "/language/{name}", method = RequestMethod.GET)
    public Language getByName(@PathVariable(value = "name") String name) {
        return languageRepository.findByName(name);
    }

    @ResponseBody
    @RequestMapping(
            value = "/language",
            method = RequestMethod.POST,
            produces = {"application/json;charset=UTF-8"}
    )
    public ActionResponse add(@Valid @RequestBody Language language) {
        if (languageRepository.findByName(language.getName()) == null) {
            languageRepository.save(language);
            return new ActionResponse("ok", language);
        }

        return new ActionResponse("error", language);
    }

    @ResponseBody
    @RequestMapping(value = "/language/{name}", method = RequestMethod.PUT, produces = {"application/json;charset=UTF-8"})
    public ActionResponse update(@PathVariable(value = "name") String name, @Valid @RequestBody UpdateRequest updateRequest) {
        Language language = languageRepository.findByName(name);
        logger.info("name " + name);
        if (language == null) {
            return new ActionResponse("error");
        }

        if (updateRequest.getDescription() != null) {
            language.setDescription(updateRequest.getDescription());
        }

        if (updateRequest.getRating() != null) {
            language.setRating(updateRequest.getRating());
        }

        languageRepository.save(language);
        return new ActionResponse("ok", language);
    }

    @RequestMapping(value = "/language/{name}", method = RequestMethod.DELETE)
    public ActionResponse deleteByName(@PathVariable(value = "name") String name) {
        languageRepository.delete(languageRepository.findByName(name));
        return new ActionResponse("ok");
    }
}
