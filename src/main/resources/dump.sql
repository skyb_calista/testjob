CREATE DATABASE test CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
CREATE USER 'test'@'localhost' IDENTIFIED BY '0AM5knQe';
GRANT ALL PRIVILEGES ON * . * TO 'test'@'localhost';
FLUSH PRIVILEGES;

CREATE TABLE `language` (
                            `id` bigint(20) NOT NULL,
                            `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                            `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                            `rating` int(11) DEFAULT NULL,
                            PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci